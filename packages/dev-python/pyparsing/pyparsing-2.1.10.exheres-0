# Copyright 2012-2016 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi setup-py [ import=distutils ]

SUMMARY="A Python Parsing Module"
DESCRIPTION="
The pyparsing module is an alternative approach to creating and executing simple
grammars, vs. the traditional lex/yacc approach, or the use of regular expressions.
The pyparsing module provides a library of classes that client code uses to construct
the grammar directly in Python code.
"
HOMEPAGE="https://${PN}.wikispaces.com"

BUGS_TO="philantrop@exherbo.org"
REMOTE_IDS="pypi:${PN}"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""

# Needs a five-year-old piece of unreleased python nowadays... Checked: 2.1.8
RESTRICT="test"

DEPENDENCIES="
    test:
        dev-python/nose[python_abis:*(-)?]
"

pkg_preinst() {
    if has_version 'dev-python/pyparsing[<=2.1.8]' ; then
        for abi in ${PYTHON_FILTERED_ABIS};  do
            if [[ -d "${ROOT}"/usr/$(exhost --target)/lib/python${abi}/site-packages/${PNV}-py${abi}.egg-info ]]; then
                 edo rm -r "${ROOT}"/usr/$(exhost --target)/lib/python${abi}/site-packages/${PNV}-py${abi}.egg-info
            fi
        done
    fi
}

