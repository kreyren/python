# Copyright 2014 Calvin Walton <calvin.walton@kepstin.ca>
# Distributed under the terms of the GNU General Public License v2

require pypi setup-py [ import=setuptools test=pytest ]

SUMMARY="Foreign Function Interface for Python calling C code"
DESCRIPTION="
The aim of this project is to provide a convenient and reliable way of
calling C code from Python. The interface is based on LuaJIT’s FFI
"
HOMEPAGE+=" https://${PN}.readthedocs.org"

LICENCES="MIT"
SLOT="0"
PLATFORMS="~amd64 ~armv7 ~armv8 ~x86"
MYOPTIONS=""

DEPENDENCIES="
    build:
        virtual/pkg-config
    build+run:
        dev-libs/libffi
        dev-python/pycparser[>=2.6][python_abis:*(-)?]
"

PYTEST_PARAMS=( c testing )

prepare_one_multibuild() {
    setup-py_prepare_one_multibuild

    edo sed \
        -e "s/cc/${CC}/" \
        -i testing/cffi0/test_ownlib.py

    # requires internet access
    edo rm testing/cffi0/test_zintegration.py
}

test_one_multibuild() {
    # cffi tries to use cc, which we banned to ensure that the
    # correct binaries are used. This should be fixed in cffi's code,
    # but here we hack around it so that we can just run the tests
    edo mkdir horrible-hack
    edo ln -s /usr/$(exhost --target)/bin/${CC} "${PWD}/horrible-hack/cc"

    export PATH="${PWD}/horrible-hack/:$PATH"

    setup-py_test_one_multibuild
}

