# Copyright 2010-2016 Wulf C. Krueger <philantrop@exherbo.org>
# Distributed under the terms of the GNU General Public License v2
# Based in part upon 'sip-4.7.9.ebuild' from Gentoo, which is:
#     Copyright 1999-2008 Gentoo Foundation

require pypi [ pn=SIP ]
require python [ blacklist='none' min_versions='2.3' multiunpack=true ]

MY_MAJORVER=$(ever major)
MY_PNV=${PN}${MY_MAJORVER}

SUMMARY="A tool for generating bindings for C++ classes so that they can be used by Python"
DESCRIPTION="
SIP is a tool to generate C++ interface code for Python. It is similar to SWIG,
but uses a different interface format. It was used to build PyQt and PyKDE, and
has support for the Qt signal/slot mechanism.
"
BASE_URI="https://www.riverbankcomputing.com"
HOMEPAGE="${BASE_URI}/software/${PN}/intro"
DOWNLOADS="${BASE_URI}/static/Downloads/${PN}/${PV}/${PNV}.tar.gz"

BUGS_TO="philantrop@exherbo.org"

UPSTREAM_CHANGELOG="${BASE_URI}/static/Downloads/${MY_PNV}/ChangeLog"
UPSTREAM_DOCUMENTATION="${BASE_URI}/static/Docs/${MY_PNV}/index.html [[ lang = en ]]"
UPSTREAM_RELEASE_NOTES="${BASE_URI}/news/${PN}-${MY_MAJORVER}$(ever range 2)"

LICENCES="|| ( GPL-2 GPL-3 SIP )"
SLOT="0"
MYOPTIONS="debug"

DEPENDENCIES=""

configure_one_multibuild(){
    # wrt '--sip-module PyQt5.sip': creates a "private" copy of the sip module
    # needed by PyQt >= 5.11, but it works for everything else I tested. Link:
    # https://www.riverbankcomputing.com/pipermail/pyqt/2018-June/040421.html
    edo "${PYTHON}" configure.py \
        --bindir "/usr/$(exhost --target)/bin" \
        --destdir "$(python_get_sitedir)" \
        --incdir "$(python_get_incdir)" \
        --sipdir "/usr/share/sip" \
        --sip-module PyQt5.sip \
        $(option debug && echo "-u") \
        CXXFLAGS_RELEASE="" CFLAGS_RELEASE="" LFLAGS_RELEASE="" \
        CFLAGS="${CFLAGS}" CXXFLAGS="${CXXFLAGS}" LFLAGS="${LDFLAGS}" \
        CC="${CC}" CXX="${CXX}" \
        LINK="${CXX}" LINK_SHLIB="${CXX}" \
        STRIP="true"
}

install_one_multibuild() {
    default
    python_bytecompile

    # Do not install plaintext sources under html/_sources/
    docinto html
    dodoc doc/html/*.*
    docinto html/_static
    dodoc doc/html/_static/*.*
}

