# Copyright 2009, 2012 Ali Polatel <alip@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require pypi setup-py [ blacklist="2" import=setuptools test=nose \
    has_bin=true python_opts="[readline][sqlite]" ]

SUMMARY="Interactive computing environment for Python"
DESCRIPTION="
IPython provides a rich architecture for interactive computing with:

- A powerful interactive shell.
- A kernel for Jupyter.
- Support for interactive data visualization and use of GUI toolkits.
- Flexible, embeddable interpreters to load into your own projects.
- Easy to use, high performance tools for parallel computing.
"
HOMEPAGE="https://ipython.org/"

LICENCES="BSD-3"
SLOT="0"
MYOPTIONS="
    matplotlib [[ description = [ Install matplotlib to provide plotting capabilities ] ]]
    nbconvert [[ description = [ Tool to convert notebooks to other formats ] ]]
    notebook [[ description = [ Install the web-based notebook backend ] ]]
    parallel [[ description = [ Enable parallel distributed computing features ] ]]
    qtconsole [[ description = [ Qt-based console with rich media output ] ]]
"

DEPENDENCIES="
    build+run:
        dev-python/backcall[python_abis:*(-)?]
        dev-python/decorator[python_abis:*(-)?]
        dev-python/pexpect[python_abis:*(-)?]
        dev-python/pickleshare[python_abis:*(-)?]
        dev-python/prompt_toolkit[>=2.0.0&<2.1.0][python_abis:*(-)?]
        dev-python/Pygments[python_abis:*(-)?]
        dev-python/simplegeneric[>0.8][python_abis:*(-)?]
        dev-python/traitlets[>=4.2][python_abis:*(-)?]
        dev-util/jedi[>=0.10][python_abis:*(-)?]
        matplotlib? (
            dev-python/matplotlib[python_abis:*(-)?]
        )
    post:
        nbconvert? (
            dev-python/nbconvert[python_abis:*(-)?]
        )
        notebook? (
            dev-python/ipywidgets[python_abis:*(-)?]
            dev-python/notebook[python_abis:*(-)?]
        )
        parallel? (
            dev-python/ipykernel[python_abis:*(-)?]
            dev-python/ipyparallel[python_abis:*(-)?]
        )
        qtconsole? (
            dev-python/ipykernel[python_abis:*(-)?]
            dev-python/qtconsole[python_abis:*(-)?]
        )
    test:
        dev-python/nbformat[python_abis:*(-)?]
        dev-python/numpy[python_abis:*(-)?]
        dev-python/requests[python_abis:*(-)?]
        dev-python/testpath[python_abis:*(-)?]
    suggestion:
        dev-python/ipykernel[python_abis:*(-)?] [[ description = [ test requirement ] ]]
"

BUGS_TO="alip@exherbo.org"

test_one_multibuild() {
    if ! has_version "dev-python/ipykernel[python_abis:*(-)?]"; then
        ewarn "Test dependencies are not installed yet, skip tests"
        return
    fi

    # Install ipython to a temporary directory for testing.
    local ipython_test_dir="${TEMP}"$(python_get_abi)/test
    edo "${PYTHON}" setup.py install \
        --home="${ipython_test_dir}" \
        --no-compile

    # iptest cannot be run inside ipython source tree
    edo pushd ${ipython_test_dir}
    DISPLAY= PYTHONPATH="$PWD/lib/python" edo "${PYTHON}" bin/iptest -j --
    edo popd
}

